<?php

/* @var $this \yii\web\View */
/* @var $content string */

//use app\widgets\Alert;
use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <link rel="stylesheet" href="/themes/components/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/themes/xtetis/css/common.css">
    common.css
    
    <?php $this->head() ?>
    

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">



<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand navbar-brand-xtetis" href="/"><span class="first_letter">X</span>Tetis</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Главная <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Проекты</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Обо мне</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Резюме</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Контакты</a>
      </li>
      
     
    </ul>
    <ul class="navbar-nav">
      <li>
      	<a>
      		<img src="/themes/xtetis/img/ico/hh.png" width="30" height="30">
      	</a>
      </li>
      <li>
      	<a>
      		<img src="/themes/xtetis/img/ico/fb.png" width="30" height="30">
      	</a>
      </li>
      <li>
      	<a>
      		<img src="/themes/xtetis/img/ico/vk.png" width="30" height="30">
      	</a>
      </li>
    </ul>

  </div>
</nav>

    <?php
    /*
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    */
    ?>

    <div class="container">
        <?//= Breadcrumbs::widget([  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
        <?//= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>


<script src="/themes/components/jquery/jquery-3.2.1.min.js"></script>
<script src="/themes/components/popper/popper.min.js"></script>
<script src="/themes/components/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
<?php $this->endPage() ?>
